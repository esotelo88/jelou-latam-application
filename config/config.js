const config = {
  development: {
    username: "root",
    password: "eduardo2024!",
    database: "jelou_user",
    host: "mariadb",
    dialect: "mysql",
    port: "3306"
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.HOST,
    dialect: process.env.DB_TYPE,
    port: "3306"
  },
};

module.exports = config;
