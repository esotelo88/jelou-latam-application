# Usa la imagen oficial de Node.js 18 como base
FROM node:18

# Instala PM2 globalmente utilizando npm
RUN npm install pm2 -g

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /usr/src/app

# Copia el archivo package.json y package-lock.json (si existe)
COPY package*.json ./

# Instala las dependencias del proyecto
RUN npm install

# Copia entrypoint
COPY .devops/entry.sh /entry.sh

# Copia el resto de la aplicación
COPY . .
RUN chmod 777 .devops/entry.sh

# Expone el puerto 3000 en el contenedor
EXPOSE 3000

# Ejecuta PM2 para iniciar la aplicación
#CMD ["pm2-runtime", "npm start"]
ENTRYPOINT [".devops/entry.sh"]





