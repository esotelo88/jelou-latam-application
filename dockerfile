# Usa una imagen base de Node.js 18 en Alpine
FROM node:18

# Establece el directorio de trabajo en /usr/src/app
WORKDIR /var/www/app

# Copia el package.json y el package-lock.json al directorio de trabajo
COPY package*.json ./

# Instala las dependencias de la aplicación
RUN npm install

EXPOSE 3000


